#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main( int argc, char **argv ) {

	if( argc != 3 ) {
		printf( "%s: [env variable] [executable name - including ./]\n", argv[0] );
		fflush( stdout );
		return EXIT_FAILURE;
	}

	char *p = getenv( argv[1] );
	p += ( strlen(argv[0]) - strlen(argv[2]) )*2;
	printf( "%s|%p\n", argv[1], p );
	fflush( stdout );
	return EXIT_SUCCESS;
}
